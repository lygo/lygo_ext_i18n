package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_i18n/lygo_i18n"
	"fmt"
	"golang.org/x/text/language"
	"testing"
)

func TestBundle(t *testing.T) {

	bundle, err := lygo_i18n.NewBundle(language.English)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = bundle.LoadAll("./i18n")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	data, err := bundle.GetDictionaryByTag(language.Italian)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(language.Italian, lygo_json.Stringify(data))

	data, err = bundle.GetDictionaryByTag(language.English)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(language.English, lygo_json.Stringify(data))

	data, err = bundle.GetDictionaryByTag(language.Japanese)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(language.Japanese, lygo_json.Stringify(data))

	res, err := bundle.Get("it-IT", "label1", nil)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if len(res) == 0 {
		t.Error("Expected a value for label1")
		t.FailNow()
	}
	fmt.Println(res)

	res, err = bundle.GetWithPlural("it", "label1", nil, map[string]interface{}{
		"count": 10,
		"name":"Angelo",
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if len(res) == 0 {
		t.Error("Expected a value for label1")
		t.FailNow()
	}
	fmt.Println(res)

	res, err = bundle.GetWithPlural("it", "label2", nil, map[string]interface{}{
		"count": 10,
		"name":"Angelo",
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if len(res) == 0 {
		t.Error("Expected a value for label2")
		t.FailNow()
	}
	fmt.Println(res)

	res, err = bundle.GetWithPlural("it", "my-group.label2", nil, map[string]interface{}{
		"count": 10,
		"name":"Angelo",
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if len(res) == 0 {
		t.Error("Expected a value for my-group.label2")
		t.FailNow()
	}
	fmt.Println(res)
}
