package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_i18n/lygo_i18n"
	"fmt"
	"testing"
)

func TestDeserialize(t *testing.T) {

	elements, err := lygo_i18n.DeserializeFile("./i18n/en.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if len(elements)==0{
		t.Error("Expected some elements")
		t.FailNow()
	}

	fmt.Println(lygo_json.Stringify(elements))

}
