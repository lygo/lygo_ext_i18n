package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_i18n/lygo_i18n"
	"fmt"
	"testing"
)

func TestTemplate(t *testing.T) {
	html, err := lygo_io.ReadTextFromFile("./template/template.html")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if len(html) == 0 {
		t.Error("HTML file should not be empty")
		t.FailNow()
	}

	model, err := lygo_json.ReadMapFromFile("./template/model.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(lygo_json.Stringify(model))

	bundle, err := lygo_i18n.NewBundleFromDir("it", "./template")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	localizer := lygo_i18n.NewLocalizer(bundle)
	fmt.Println(localizer)
	localized, err := localizer.Localize("it", html, model)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(localized)
}
