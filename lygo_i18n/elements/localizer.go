package elements

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_ext_i18n/lygo_i18n/commons"
	"github.com/cbroglie/mustache"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e
//----------------------------------------------------------------------------------------------------------------------

type Localizer struct {
	bundle *Bundle
}

func NewLocalizer(bundle *Bundle) *Localizer {
	instance := new(Localizer)
	instance.bundle = bundle
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Localizer) String() string {
	if nil != instance.bundle {
		return instance.bundle.String()
	}
	return ""
}

func (instance *Localizer) Localize(rawLang string, text string, context ...interface{}) (string, error) {
	// consolidate the context into a single model
	model := toMap(context...)

	// localize the model
	localizeModel(&model, rawLang, instance.bundle)

	// get all template tags
	// all missing tags should be added to model and retrieved from
	// an i18n bundle
	tpl, err := mustache.ParseString(text)
	if nil != err {
		return "", err
	}

	// add missing retrieving from bundle
	missing := getMissingModelKeys(tpl, model)
	for _, key := range missing {
		value, err := instance.bundle.GetWithPlural(rawLang, key, nil, model)
		if nil == err {
			model[key] = value
		}
	}

	// ready to render the template
	return mustache.Render(text, model)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func toMap(context ...interface{}) map[string]interface{} {
	response := make(map[string]interface{})
	for _, c := range context {
		if mm, b := c.(map[string]interface{}); b {
			for k, v := range mm {
				response[k] = v
			}
		}
	}
	return response
}

func getMissingModelKeys(tpl *mustache.Template, context ...interface{}) []string {
	response := make([]string, 0)
	tags := getTags(tpl)
	keys := getKeys(true, context...)
	for _, tag := range tags {
		if lygo.Arrays.IndexOf(tag.Name(), keys) == -1 {
			response = append(response, tag.Name())
		}
	}
	return response
}

func tagsFrom(text string) ([]mustache.Tag, error) {
	tpl, err := mustache.ParseString(text)
	if nil != err {
		return nil, err
	}
	return getTags(tpl), nil
}

func getTags(tpl *mustache.Template) []mustache.Tag {
	response := make([]mustache.Tag, 0)
	deepTags(tpl.Tags(), &response)
	return response
}

func deepTags(tags []mustache.Tag, response *[]mustache.Tag) {
	for _, tag := range tags {
		switch tag.Type() {
		case mustache.Section, mustache.InvertedSection:
			deepTags(tag.Tags(), response)
		default:
			*response = append(*response, tag)
		}
	}
}

func getKeys(allowDuplicates bool, context ...interface{}) []string {
	response := make([]string, 0)
	for _, c := range context {
		if mm, b := c.(map[string]interface{}); b {
			deepKeys(mm, &response, allowDuplicates)
		}
	}
	return response
}

func deepKeys(m map[string]interface{}, response *[]string, allowDuplicates bool) {
	for k, v := range m {
		if mm, b := v.(map[string]interface{}); b {
			deepKeys(mm, response, allowDuplicates)
		} else if a, b := v.([]map[string]interface{}); b && len(a) > 0 {
			deepKeys(a[0], response, allowDuplicates)
		} else {
			if !allowDuplicates && lygo.Arrays.IndexOf(k, *response) == -1 {
				*response = append(*response, k)
			} else {
				*response = append(*response, k)
			}
		}
	}
}

// localizeModel loop on all model data and check for tags to resolve.
// localization data are retrieved from i18n bundle
func localizeModel(pmodel *map[string]interface{}, lang string, bundle *Bundle) {
	model := *pmodel
	for k, v := range model {
		if s, b := v.(string); b {
			value, err := localize(s, lang, bundle, model)
			if nil == err && s != value {
				model[k] = value
			}
		} else if m, b := v.(map[string]interface{}); b {
			localizeModel(&m, lang, bundle)
		} else if a, b := v.([]map[string]interface{}); b {
			// extend model with array counter
			model[k+commons.SUFFIX_LENGHT] = len(a)
			for mi, mm := range a {
				mm[k+commons.SUFFIX_INDEX] = mi
				localizeModel(&mm, lang, bundle)
			}
		} else if a, b := v.([]interface{}); b {
			// extend model with array counter
			model[k+commons.SUFFIX_LENGHT] = len(a)
			for iii, ii := range a {
				if mm, b:= ii.(map[string]interface{}); b {
					mm[k+commons.SUFFIX_INDEX] = iii
					localizeModel(&mm, lang, bundle)
				}
			}
		}
	}
}

func localize(text string, lang string, bundle *Bundle, i18nModel map[string]interface{}) (string, error) {
	tags, err := tagsFrom(text)
	if nil != err {
		return "", err
	}
	if len(tags) > 0 {
		model := make(map[string]interface{})
		for _, tag := range tags {
			value, err := bundle.GetWithPlural(lang, tag.Name(), nil, i18nModel)
			if nil == err && len(value) > 0 {
				model[tag.Name()] = value
			}
		}
		return mustache.Render(text, model)
	}
	return text, nil
}
