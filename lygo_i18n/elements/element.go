package elements

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_i18n/lygo_i18n/commons"
	"github.com/cbroglie/mustache"
	"strings"
)

var (
	PLURAL_FIELDS = []string{"value", "count", "values", "qty", "length", "len", "size", "num"}
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e
//----------------------------------------------------------------------------------------------------------------------

type Deserializer func(data []byte) (map[string]*Element, error)

type Element struct {
	Zero  string `json:"zero"`
	One   string `json:"one"`
	Two   string `json:"two"`
	Other string `json:"other"`
}

func NewElementFromString(s string) *Element {
	return &Element{
		Zero:  s,
		One:   s,
		Two:   s,
		Other: s,
	}
}

func NewElementFromMap(m map[string]interface{}) (*Element, error) {
	var instance Element
	err := lygo_json.Read(lygo_conv.ToString(m), &instance)
	if nil != err {
		return nil, err
	}
	if len(instance.Two) == 0 {
		instance.Two = instance.Other
	}
	if len(instance.Zero) == 0 {
		instance.Zero = instance.Other
	}

	return &instance, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Element) Value() string {
	if len(instance.One) > 0 {
		return instance.One
	} else if len(instance.Two) > 0 {
		return instance.Two
	} else if len(instance.Zero) > 0 {
		return instance.Zero
	}
	return instance.Other
}

func (instance *Element) Plural(value float64) string {
	switch value {
	case -1:
		return instance.Value()
	case 0:
		return instance.Zero
	case 1:
		return instance.One
	case 2:
		return instance.Two
	default:
		return instance.Other
	}
}

func (instance *Element) Get(model ...interface{}) string {
	text := instance.Value()
	return render(text, model...)
}

func (instance *Element) GetWithPlural(pluralFields []string, model ...interface{}) string {
	if nil == pluralFields {
		pluralFields = PLURAL_FIELDS
	}
	value := lookUpPluralValue(pluralFields, model...)
	return instance.GetWithPluralValue(value, model...)
}

func (instance *Element) GetWithPluralValue(value float64, model ...interface{}) string {
	text := instance.Plural(value)
	return render(text, model...)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func lookUpPluralValue(pluralFields []string, model ...interface{}) float64 {
	result := float64(-1)
	for _, m := range model {
		if mm, b := m.(map[string]interface{}); b {
			for k, v := range mm {
				if isPluralField(k, pluralFields) {
					result = lygo_conv.ToFloat64(v)
					goto exit
				}
				if b, v := lygo_conv.IsNumeric(v); b {
					result = v
				}
			}
		}
	}
exit:
	return result
}

func isPluralField(name string, pluralFields []string) bool {
	if lygo.Arrays.IndexOf(name, pluralFields) > -1 {
		return true
	}
	if strings.Index(name, commons.SUFFIX_SEP) > -1 {
		name := lygo_conv.ToString(lygo.Arrays.GetAt(strings.Split(name, commons.SUFFIX_SEP), 1, ""))
		return isPluralField(name, pluralFields)
	}
	return false
}

func render(text string, model ...interface{}) string {
	res, err := mustache.Render(text, model...)
	if nil != err {
		return text
	}
	return res
}
