package commons

import "errors"

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

const (
	SUFFIX_SEP    = "-"
	SUFFIX_LENGHT = "-length"
	SUFFIX_INDEX  = "-index"
)

//----------------------------------------------------------------------------------------------------------------------
//	e r r o r s
//----------------------------------------------------------------------------------------------------------------------

var (
	LanguageNotSupportedError      = errors.New("language_not_supported")
	DeserializerNotRegisteredError = errors.New("deserializer_not_registered")
	ResourceNotFoundError          = errors.New("resource_not_found")
)
