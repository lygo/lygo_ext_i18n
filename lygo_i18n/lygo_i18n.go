package lygo_i18n

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_i18n/lygo_i18n/commons"
	"bitbucket.org/lygo/lygo_ext_i18n/lygo_i18n/elements"
	"golang.org/x/text/language"
)

var deserializers map[string]elements.Deserializer

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r s
//----------------------------------------------------------------------------------------------------------------------

func NewBundle(lang interface{}) (*elements.Bundle, error) {
	if t, b := lang.(language.Tag); b {
		return elements.NewBundleFromTag(t, &deserializers), nil
	} else if s, b := lang.(string); b {
		return elements.NewBundle(s, &deserializers)
	}
	return nil, commons.LanguageNotSupportedError
}

func NewBundleFromDir(lang interface{}, path string) (*elements.Bundle, error) {
	bundle, err := NewBundle(lang)
	if nil != err {
		return nil, err
	}
	err = bundle.LoadAll(path)
	if nil != err {
		return nil, err
	}
	return bundle, nil
}

func NewBundleFromFiles(lang interface{}, files []string) (*elements.Bundle, error) {
	bundle, err := NewBundle(lang)
	if nil != err {
		return nil, err
	}
	err = bundle.LoadFiles(files)
	if nil != err {
		return nil, err
	}
	return bundle, nil
}

func NewLocalizer(bundle *elements.Bundle) *elements.Localizer {
	return elements.NewLocalizer(bundle)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func RegisterDeserializer(extension string, deserializer elements.Deserializer) {
	deserializers[extension] = deserializer
}

func DeserializeFile(filename string) (map[string]*elements.Element, error) {
	if b, err := lygo_paths.Exists(filename); b {
		ext := lygo_paths.ExtensionName(filename)
		if deserializer, b := deserializers[ext]; b {
			data, err := lygo_io.ReadBytesFromFile(filename)
			if nil != err {
				return nil, err
			}
			return deserializer(data)
		} else {
			return nil, commons.DeserializerNotRegisteredError
		}
	} else {
		return nil, err
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func init() {
	deserializers = make(map[string]elements.Deserializer)
	deserializers["json"] = deserializeJSON
}

func deserializeJSON(data []byte) (map[string]*elements.Element, error) {
	response := make(map[string]*elements.Element)
	var m map[string]interface{}
	err := lygo_json.Read(data, &m)
	if nil == err {
		parse("", m, response)
	}
	return response, err
}

func parse(parentKey string, m map[string]interface{}, data map[string]*elements.Element) {
	for k, v := range m {
		key := parentKey
		if len(key) > 0 {
			key += "."
		}
		key += k
		if mm, b := v.(map[string]interface{}); b {
			// test if is an element
			elem, err := elements.NewElementFromMap(mm)
			if nil == err && len(elem.One) > 0 {
				data[key] = elem
			} else {
				parse(key, mm, data)
			}
		} else if s, b := v.(string); b {
			// from simple string
			elem := elements.NewElementFromString(s)
			data[key] = elem
		}
	}
}
